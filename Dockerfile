FROM scratch
EXPOSE 8080
ENTRYPOINT ["/readygo"]
COPY ./bin/ /